<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field;

use Drupal;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Field\FieldDefinitionInterface;
use function implode;

/**
 * Class UniqueEntityField
 */
abstract class UniqueBase {
  const MODULE = 'unique_entity_field';
  const SETTINGS = 'unique_entity_field.settings';
  const FIELDS = 'fields';
  const SCOPE = 'scope';
  //  const LANGUAGE = 'language';

  protected $entityType;
  protected $bundle;

  /**
   * @param string $entityType
   * @param string $bundle
   */
  function __construct(string $entityType, string $bundle) {
    $this->entityType = $entityType;
    $this->bundle = $bundle;
  }

  /**
   * @param bool $withMachineName
   *
   * @return FieldDefinitionInterface[]
   */
  function getUniqueFields(bool $withMachineName): array {
    $items = [];

    /** @var EntityFieldManager $manager */
    $manager = Drupal::service('entity_field.manager');

    /** @var FieldDefinitionInterface[] $fields */
    $fields = $manager->getFieldDefinitions($this->entityType, $this->bundle);

    foreach ($fields as $name => $field) {
      if (false === $this->canUniqueField($field)) {
        continue;
      }

      $label = $field->getLabel();
      $suffix = $withMachineName ? ' (' . $field->getName() . ')' : '';
      $items[$name] = $label . $suffix;
    }

    unset($items['type']);
    return $items;
  }

  /**
   * @param FieldDefinitionInterface $field
   *
   * @return bool
   */
  function canUniqueField(FieldDefinitionInterface $field): bool {
    if ($field->getFieldStorageDefinition()->isMultiple()) {
      return false;
    }

    if (false === $field->isRequired()) {
      return false;
    }

    return true;
  }

  /**
   * @param string $type
   *
   * @return string
   */
  function getConfigKey(string $type) {
    $items = [static::SETTINGS, $type, $this->entityType, $this->bundle];
    return implode('.', $items);
  }

  /**
   * @param string $type
   *
   * @return mixed
   */
  function getConfig(string $type) {
    return Drupal::configFactory()->get(static::SETTINGS)->get($this->getConfigKey($type));
  }

  /**
   * @param string $type
   * @param mixed  $value
   *
   * @return static
   */
  function setConfig(string $type, $value) {
    Drupal::configFactory()->getEditable(static::SETTINGS)->set($this->getConfigKey($type), $value)->save();
    return $this;
  }

  /**
   * @return array
   */
  function getScopeOptions() {
    return [
      'type' => t('Bundle'),
      'langcode' => t('Language')
    ];

  }

}
