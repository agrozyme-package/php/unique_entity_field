<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field\EventSubscriber\FormBaseAlter;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\hook_event_dispatcher\Event\Form\FormBaseAlterEvent as Event;
use Drupal\unique_entity_field\UniqueValidation;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NodeForm
 */
class NodeForm implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    return [
      'hook_event_dispatcher.form_base_node_form.alter' => 'alter',
    ];
  }

  /**
   * @param Event $event
   */
  function alter(Event $event) {
    /** @var EntityFormInterface $formObject */
    $formObject = $event->getFormState()->getFormObject();

    if (false === $formObject instanceof ContentEntityFormInterface) {
      return;
    }

    $entity = $formObject->getEntity();
    $entityType = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $form = &$event->getForm();
    $validation = new UniqueValidation($entityType, $bundle);
    $validation->alter($event);
  }

}
