<?php
declare(strict_types=1);

namespace Drupal\unique_entity_field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\hook_event_dispatcher\Event\Form\BaseFormEvent as Event;

/**
 * Class SettingsForm
 */
class UniqueSettingsForm extends UniqueBase {

  /**
   * @param Event $event
   */
  function alter(Event $event) {
    $form = &$event->getForm();
    $defaultFields = $this->getConfig(static::FIELDS);
    $defaultScope = $this->getConfig(static::SCOPE);

    $form[static::MODULE] = [
      '#type' => 'details',
      '#title' => t('Unique Entity Field restrictions'),
      '#group' => 'additional_settings',
      '#tree' => true,
    ];

    $element = &$form[static::MODULE];

    $element[static::FIELDS] = [
      '#type' => 'checkboxes',
      '#title' => t('Choose the fields that should be unique'),
      '#options' => $this->getUniqueFields(true),
      '#default_value' => is_array($defaultFields) ? $defaultFields : [],
      '#description' => t('After designating that certain fields should be unique, users will not be able to submit the content form to create a new node or update an existing one if it contains values in the designated fields that duplicate others.'),
    ];

    $options = $this->getScopeOptions();
    $defaultScope = isset($options[$defaultScope]) ? $defaultScope : reset($options);

    $element[static::SCOPE] = [
      '#type' => 'radios',
      '#title' => t('Choose the scope for the unique values'),
      '#options' => $options,
      '#default_value' => $defaultScope,
      '#description' => t('Choose whether the values in the specified fields must be unique among nodes of this content type, among nodes of the same language, among all nodes, or only among the fields of the present node.'),
    ];

    $form['actions']['submit']['#submit'][] = [$this, 'submit'];
  }

  /**
   * @param array              $element
   * @param FormStateInterface $form_state
   */
  function submit(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $items = [static::FIELDS, static::SCOPE];

    foreach ($items as $item) {
      $value = $values[static::MODULE][$item] ?? [];
      $this->setConfig($item, $value);
    }

  }

}
